#!/bin/bash
# LICENSE CDDL 1.0 + GPL 2.0
#
# Copyright (c) 1982-2016 Oracle and/or its affiliates. All rights reserved.
# 
# Since: November, 2016
# Author: gerald.venzl@oracle.com
# Description: Installs new Perl binaries if not present
# 
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
# 

# Install latest Perl
cd /opt
wget http://www.cpan.org/src/5.0/perl-5.14.1.tar.gz -O /opt/perl-5.14.1.tgz
tar zxvf /opt/perl-5.14.1.tgz -C /opt/
cd /opt/perl-5.14.1
./Configure -des -Dprefix=$ORACLE_CLIENT_HOME/perl -Doptimize=-O3 -Dusethreads -Duseithreads -Duserelocatableinc
make clean
make
make install

cd $ORACLE_CLIENT_HOME/lib
ln -sf ../javavm/jdk/jdk7/lib/libjavavm12.a

# Cleanup
rm -rf /opt/perl-src*
