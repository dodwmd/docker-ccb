You must download the following files and place them in files/

Using the following URL: http://www.oracle.com/technetwork/topics/linuxx86-64soft-092277.html

Download oracle-instantclient12.1-basic-12.1.0.2.0-1.x86_64.rpm oracle-instantclient12.1-devel-12.1.0.2.0-1.x86_64.rpm oracle-instantclient12.1-sqlplus-12.1.0.2.0-1.x86_64.rpm

Using the following URL: http://www.oracle.com/technetwork/middleware/weblogic/downloads/wls-for-dev-1703574.html

Download the generic installer (fmw_12.2.1.2.0_wls_Disk1_1of1.zip) and uncompress. Place the file fmw_12.2.1.2.0_wls.jar in files/
