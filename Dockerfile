FROM oraclelinux:7
MAINTAINER Michael Dodwell <michael@dodwell.us>

# Install required system packages
RUN yum install -y wget \
                   libaio.x86_64 \
                   glibc.x86_64 \
                   unzip \
                   oracle-rdbms-server-12cR1-preinstall \
                   expat-devel

# Download server JRE
RUN wget --no-check-certificate -c --header "Cookie: oraclelicense=accept-securebackup-cookie" \
    http://download.oracle.com/otn-pub/java/jdk/8u121-b13/e9e7ea248e2c4826b92b3f075a80e441/server-jre-8u121-linux-x64.tar.gz \
    -O /server-jre.tgz

# unpack and setup Server Java JRE

RUN mkdir -p /usr/java && \
    tar zxvf /server-jre.tgz -C /usr/java && \
    export JAVA_DIR=$(ls -1 -d /usr/java/*) && \
    ln -s $JAVA_DIR /usr/java/latest && \
    ln -s $JAVA_DIR /usr/java/default && \
    alternatives --install /usr/bin/java java $JAVA_DIR/bin/java 20000 && \
    alternatives --install /usr/bin/javac javac $JAVA_DIR/bin/javac 20000 && \
    alternatives --install /usr/bin/jar jar $JAVA_DIR/bin/jar 20000

# Environment variables required for this build (do NOT change)
ENV FMW_JAR=fmw_12.2.1.2.0_wls.jar \
    SCRIPT_FILE=/opt/oracle/createAndStartEmptyDomain.sh \
    ORACLE_HOME=/opt/oracle \
    USER_MEM_ARGS="-Djava.security.egd=file:/dev/./urandom" \
    DEBUG_FLAG=true \
    PRODUCTION_MODE=dev \
    DOMAIN_NAME="${DOMAIN_NAME:-base_domain}" \
    DOMAIN_HOME=/opt/oracle/user_projects/domains/${DOMAIN_NAME:-base_domain} \
    ADMIN_PORT="${ADMIN_PORT:-8001}" \
    PATH=$PATH:/usr/java/default/bin:/opt/oracle/oracle_common/common/bin:/opt/oracle/wlserver/common/bin

# Copy packages
# -------------
COPY files/$FMW_JAR files/install.file files/oraInst.loc /opt/
COPY files/createAndStartEmptyDomain.sh files/create-wls-domain.py /opt/oracle/

# Setup filesystem and oracle user
# Install and configure Oracle JDK
# Adjust file permissions, go to /opt as user 'oracle' to proceed with WLS installation
# 
#    useradd -b /opt -M -s /bin/bash oracle && \
# ------------------------------------------------------------
RUN chmod a+xr /opt && \
    chmod +xr $SCRIPT_FILE && \
    chown oracle:dba -R /opt && \
    echo oracle:oracle | chpasswd && \
    sed -e '/memlock/ s/^#*/#/' -i /etc/security/limits.d/oracle-rdbms-server-12cR1-preinstall.conf && \
    su -c "$JAVA_HOME/bin/java -jar /opt/$FMW_JAR -silent -responseFile /opt/install.file -invPtrLoc /opt/oraInst.loc -jreLoc /usr/java/latest -ignoreSysPrereqs -force -novalidation ORACLE_HOME=$ORACLE_HOME INSTALL_TYPE=\"WebLogic Server\"" - oracle && \
    rm /opt/$FMW_JAR /opt/oraInst.loc /opt/install.file

RUN adduser --system --no-create-home --shell /bin/bash cisusr
RUN adduser --system --no-create-home --no-user-group --groups cisusr --shell /bin/ksh cissys

COPY files/oracle-instantclient12.1* /opt/
RUN rpm -ivh /opt/oracle-instantclient12.1* && \
    rm -f /opt/oracle-instanceclient12.1*

RUN mkdir $ORACLE_HOME/network/admin -p

COPY files/tnsnames.ora $ORACLE_HOME/network/admin/tnsnames.ora

ENV ORACLE_CLIENT_HOME=/usr/lib/oracle/12.1/client64 \
    PATH=$ORACLE_CLIENT_HOME/perl/bin:$PATH:$ORACLE_HOME/bin \
    LD_LIBRARY_PATH=$ORACLE_HOME/lib \
    TNS_ADMIN=$ORACLE_HOME/network/admin \
    HIBERNATE_JAR_DIR=/opt/hibernate-4.1.0

# Install Hibernate
RUN mkdir -p /opt/hibernate-tmp
COPY files/hibernate-release-4.1.0.Final.zip /opt/hibernate-tmp/hibernate.zip
RUN mkdir -p $HIBERNATE_JAR_DIR && \
    ln -s $HIBERNATE_JAR_DIR /opt/hibernate-default && \
    unzip /opt/hibernate-tmp/hibernate.zip -d /opt/hibernate-tmp && \
    cp /opt/hibernate-tmp/hibernate-release-4.1.0.Final/lib/optional/ehcache/ehcache-core-2.4.3.jar $HIBERNATE_JAR_DIR && \
    cp /opt/hibernate-tmp/hibernate-release-4.1.0.Final/lib/optional/ehcache/hibernate-ehcache-4.1.0.Final.jar $HIBERNATE_JAR_DIR && \
    cp /opt/hibernate-tmp/hibernate-release-4.1.0.Final/lib/required/hibernate-commons-annotations-4.0.1.Final.jar $HIBERNATE_JAR_DIR && \
    cp /opt/hibernate-tmp/hibernate-release-4.1.0.Final/lib/required/hibernate-core-4.1.0.Final.jar $HIBERNATE_JAR_DIR && \
    cp /opt/hibernate-tmp/hibernate-release-4.1.0.Final/lib/required/hibernate-jpa-2.0-api-1.0.1.Final.jar $HIBERNATE_JAR_DIR && \
    cp /opt/hibernate-tmp/hibernate-release-4.1.0.Final/lib/required/javassist-3.15.0-GA.jar $HIBERNATE_JAR_DIR && \
    cp /opt/hibernate-tmp/hibernate-release-4.1.0.Final/lib/required/jboss-logging-3.1.0.CR2.jar $HIBERNATE_JAR_DIR && \
    cp /opt/hibernate-tmp/hibernate-release-4.1.0.Final/lib/required/jboss-transaction-api_1.1_spec-1.0.0.Final.jar $HIBERNATE_JAR_DIR && \
    rm -rf /opt/hibernate-tmp

# Install Perl
COPY files/installPerl.sh /opt/
RUN /bin/bash /opt/installPerl.sh && \
    rm -f /opt/installPerl.sh && \
    curl -L http://cpanmin.us | $ORACLE_CLIENT_HOME/perl/bin/perl - App::cpanminus && \
    $ORACLE_CLIENT_HOME/perl/bin/cpanm XML::Parser

# Install Oracle Utilities Framework Application Server
RUN mkdir -p /ouaf/temp
COPY files/FW-V4.3.0.2.0-MultiPlatform.jar /ouaf/temp/
RUN unzip /ouaf/temp/FW-V4.3.0.2.0-MultiPlatform.jar -d /ouaf/temp/ && \
    touch /etc/cistab && \
    chown cissys:cisusr /etc/cistab && \
    chmod 664 /etc/cistab && \
    rm -f /ouaf/temp/FW-V4.3.0.2.0-MultiPlatform.jar

# Define default command to start script. 
WORKDIR ${ORACLE_HOME}
CMD ["/opt/oracle/createAndStartEmptyDomain.sh"]
